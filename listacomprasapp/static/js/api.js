$(function () {
    var D = new Date();
    var anno = D.getFullYear();
    var mes = D.getMonth() + 1;
    var dia = D.getDate();
    if (dia < 10) { dia = "0" + dia; }
    if (mes < 10) { mes = "0" + mes; }

    var apiDolar = new XMLHttpRequest();
    apiDolarUrl = 'https://api.sbif.cl/api-sbifv3/recursos_api/dolar/' + anno + '/' + mes + '/dias/' + dia + '?apikey=612e5f8a7393c4a5d947143780856ae3761d4666&formato=json'
    apiDolar.responseType = 'json';
    apiDolar.open('GET', apiDolarUrl, true);
    var Dolar;
    apiDolar.onload = function () {
        var Dolar = apiDolar.response;
        $(".dolar").html(Dolar.Dolares[0].Valor);
    }
    apiDolar.send(null);


    var apiUF = new XMLHttpRequest();
    Url = 'https://api.sbif.cl/api-sbifv3/recursos_api/uf/' + anno + '/' + mes + '/dias/' + dia + '?apikey=612e5f8a7393c4a5d947143780856ae3761d4666&formato=json'
    apiUF.responseType = 'json';
    apiUF.open('GET', Url, true);
    apiUF.onload = function () {
        var UF = apiUF.response;
        console.log(UF.UFs)
        $(".uf").html(UF.UFs[0].Valor);
    }
    apiUF.send(null);

    var apiEURO = new XMLHttpRequest();
    EUROUrl = 'https://api.sbif.cl/api-sbifv3/recursos_api/euro/' + anno + '/' + mes + '/dias/' + dia + '?apikey=612e5f8a7393c4a5d947143780856ae3761d4666&formato=json'
    apiEURO.responseType = 'json';
    apiEURO.open('GET', EUROUrl, true);
    apiEURO.onload = function () {
        var EURO = apiEURO.response;
        $(".euro").html(EURO.Euros[0].Valor);
    }
    apiEURO.send(null);
})