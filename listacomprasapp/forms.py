from django import forms
from . import models

class AgregarLista(forms.ModelForm):
    class Meta:
        model = models.Lista
        fields = ['nombre']

class AgregarProducto(forms.ModelForm):
    class Meta:
        model = models.Producto
        fields = ['nombre','costo_presupuestado', 'comentario']

class ComprarProducto(forms.ModelForm):
    class Meta:
        model = models.Producto
        fields = ['costo_real', 'comentario', 'sucursal']

class AgregarTienda(forms.ModelForm):
    class Meta:
        model = models.Tienda
        fields = ['nombre']

class AgregarSucursal(forms.ModelForm):
    class Meta:
        model = models.Sucursal
        fields = ['sucursal_fisica','nombre', 'direccion', 'comuna' ]