from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from . import forms
from .models import Lista, Producto, ListaProducto, Tienda, Sucursal



def home(request): #login
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request,user)
            return redirect('listacomprasapp:listas')
    else:
        form = AuthenticationForm()
    page_title = "Bichito"
    return render(request,'listacomprasapp/home.html', {'page_title': page_title, 'form':form})

def registro_usuario(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request,user)
            return redirect('listacomprasapp:listas')
    else:
        form = UserCreationForm()
    return render(request, 'listacomprasapp/registro_usuario.html', {'form' : form })

def cerrar_sesion(request):
    if request.method == 'POST':
        logout(request)
        return redirect('listacomprasapp:home')

def listas(request):
    id = request.user.id
    listas = Lista.objects.filter(duenno_id=id)
    
    return render(request, 'listacomprasapp/listas.html', {'listas':listas})

def tiendas(request):
    if request.user.is_staff:
        tiendas = Tienda.objects.all()
    else:
        tiendas = Tienda.objects.filter(estado=True)
    return render(request, 'listacomprasapp/tiendas.html', {'tiendas':tiendas})

def sucursales(request, id_tienda):
    sucursales = Sucursal.objects.filter(tienda_id=id_tienda)
    tienda = Tienda.objects.filter(id=id_tienda)
    return render(request, 'listacomprasapp/sucursales.html', {'sucursales':sucursales, 'id_tienda':id_tienda, 'tienda':tienda})

@login_required(login_url="listacomprasapp:home")
def agregar_sucursal(request, id_tienda):
    tiendas = Tienda.objects.filter(id = id_tienda)
    if request.method == 'POST':
        form = forms.AgregarSucursal(request.POST)
        if form.is_valid():
            instancia = form.save(commit=False)
            tienda = get_object_or_404(Tienda, id=id_tienda)
            instancia.tienda = tienda
            instancia.save()
            return redirect('listacomprasapp:tiendas')
    else:
        form = forms.AgregarSucursal()
    return render(request, 'listacomprasapp/agregar_sucursal.html', {'form':form,'tiendas':tiendas})

@login_required(login_url="listacomprasapp:home")
def agregar_tienda(request):
    if request.method == 'POST':
        form = forms.AgregarTienda(request.POST)
        if form.is_valid():
            instancia = form.save(commit=False)
            instancia.save()
            return redirect('listacomprasapp:tiendas')
    else:
        form = forms.AgregarTienda()
    return render(request, 'listacomprasapp/agregar_tienda.html', {'form':form})

@login_required(login_url="listacomprasapp:home")
def aprobar_tienda(request, id_tienda):
    tienda = get_object_or_404(Tienda, id=id_tienda)
    tienda.estado = True
    tienda.save(update_fields=["estado"])
    return redirect('listacomprasapp:tiendas')

@login_required(login_url="listacomprasapp:home")
def agregar_lista(request):
    if request.method == 'POST':
        form = forms.AgregarLista(request.POST)
        if form.is_valid():
            instancia = form.save(commit=False)
            instancia.duenno_id = request.user.id
            instancia.save()
            return redirect('listacomprasapp:listas')
    else:
        form = forms.AgregarLista()
    return render(request, 'listacomprasapp/agregar_lista.html', {'form':form})

@login_required(login_url="listacomprasapp:home")
def agregar_producto(request, id_lista):
    if request.method == 'POST':
        form = forms.AgregarProducto(request.POST)
        if form.is_valid():
            instancia = form.save(commit=False)
            instancia.save()

            auxLista = get_object_or_404(Lista, id=id_lista)
            lista_producto = ListaProducto(producto=instancia,lista=auxLista)
            lista_producto.save()
            return redirect('listacomprasapp:detalle_lista', id_lista=id_lista)
    else:
        form = forms.AgregarProducto()
    return render(request, 'listacomprasapp/agregar_producto.html', {'form':form})

@login_required(login_url="listacomprasapp:home")
def comprar_producto(request, id_producto=None):
    instancia = get_object_or_404(Producto, id=id_producto)
    form = forms.ComprarProducto(request.POST or None, instance=instancia)
    if form.is_valid():
        instancia = form.save(commit=False)
        instancia.estado = False #True: Pendiente, False: Comprado
        instancia.save()
        lista = Lista.objects.get(listaproducto__producto=id_producto)

        return redirect("listacomprasapp:detalle_lista", id_lista=lista.id)
    contexto = {
        "nombre":instancia.nombre,
        "costo_real":instancia.costo_real,
        "comentario":instancia.comentario,
        "sucursal":instancia.sucursal,
        "producto":instancia,
        "form":form,
    }
    return render(request, 'listacomprasapp/comprar_producto.html', contexto)

@login_required(login_url="listacomprasapp:home")
def eliminar_producto(request, id_producto=None):
    instancia = get_object_or_404(Producto, id=id_producto)
    instancia.delete()  
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@login_required(login_url="listacomprasapp:home")
def detalle_lista(request, id_lista):
    lista = Lista.objects.filter(id = id_lista)
    productos = Producto.objects.filter(listaproducto__lista=id_lista)
    total_costo_presupuestado=0
    total_costo_real=0
    productos_comprados=0
    productos_pendientes=0

    for producto in productos:
        if producto.estado == True:
            total_costo_presupuestado=total_costo_presupuestado+producto.costo_presupuestado
            productos_pendientes=productos_pendientes+1
        else:
            total_costo_real=total_costo_real+producto.costo_real
            total_costo_presupuestado=total_costo_presupuestado+producto.costo_presupuestado
            productos_comprados=productos_comprados+1

    contexto = {
        "productos":productos,
        "total_costo_presupuestado":total_costo_presupuestado,
        "total_costo_real":total_costo_real,
        "id_lista":id_lista,
        "lista":lista,
        "productos_comprados": productos_comprados,
        "productos_pendientes": productos_pendientes,
    }
    return render(request,'listacomprasapp/detalle_lista.html',contexto)