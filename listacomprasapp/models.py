from django.db import models

# Create your models here.
class Region(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

class Comuna(models.Model):
    nombre = models.CharField(max_length=100)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

class Tienda(models.Model):
    nombre = models.CharField(max_length=100, unique=True)
    estado = models.BooleanField(default=False)

    def __str__(self):
        return self.nombre

#class TipoSucursal(models.Model):
#    nombre = models.CharField(max_length=100)
#
#    def __str__(self):
#        return self.nombre

class Sucursal(models.Model):
    nombre = models.CharField(max_length=100, null=True,blank=True)
    direccion = models.CharField(max_length=100)
    tienda = models.ForeignKey(Tienda, on_delete=models.CASCADE, default=1)
    comuna = models.ForeignKey(Comuna, on_delete=models.CASCADE, null=True, blank=True)
    sucursal_fisica = models.BooleanField(default=False)

    def __str__(self):
        return self.direccion

class Producto(models.Model):
    nombre = models.CharField(max_length=100, null=False)
    costo_presupuestado = models.IntegerField(null=True, blank=True)
    costo_real = models.IntegerField(null=True, blank=True)
    comentario = models.CharField(max_length=100, null=True, blank=True)
    sucursal = models.ForeignKey(Sucursal, on_delete=models.CASCADE, null=True, blank=True)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre

class Lista(models.Model):
    nombre = models.CharField(max_length=100)
    duenno = models.ForeignKey('auth.User', on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.nombre

class ListaProducto(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    lista = models.ForeignKey(Lista, on_delete=models.CASCADE)

    def __str__(self):
        return "Lista: " + str(self.lista) + ", Producto: " + str(self.producto)

