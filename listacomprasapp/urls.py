from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings
# urls de la aplicación /sitio
 
app_name = "listacomprasapp"

urlpatterns = [
    path('', views.home, name="home"),
    path(r'registro_usuario/', views.registro_usuario, name="registro_usuario"),
    path(r'listas/', views.listas, name="listas"),
    path(r'listas/agregar_lista/', views.agregar_lista, name='agregar_lista'),
    path(r'tiendas/', views.tiendas, name="tiendas"),
    path(r'tiendas/agregar_tienda/', views.agregar_tienda, name='agregar_tienda'),
    path(r'cerrar_sesion/', views.cerrar_sesion, name="cerrar_sesion"),
    path(r'tiendas/<int:id_tienda>/agregar_sucursal/', views.agregar_sucursal, name='agregar_sucursal'),
    path(r'tiendas/<int:id_tienda>/sucursales/', views.sucursales, name="sucursales"),
    path(r'tiendas/<int:id_tienda>/aprobar_tienda/', views.aprobar_tienda, name="aprobar_tienda"),
    path(r'listas/<int:id_lista>/detalle_lista/', views.detalle_lista, name='detalle_lista'),
    path(r'listas/<int:id_producto>/comprar_producto/', views.comprar_producto, name='comprar_producto'),
    path(r'listas/<int:id_producto>/eliminar_producto/', views.eliminar_producto, name='eliminar_producto'),
    path(r'listas/<int:id_lista>/agregar_producto/', views.agregar_producto, name='agregar_producto'),
]