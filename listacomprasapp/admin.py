from django.contrib import admin
from .models import Region
from .models import Comuna
from .models import Tienda
#from .models import TipoSucursal
from .models import Sucursal
from .models import Producto
from .models import Lista
from .models import ListaProducto

# Register your models here.
admin.site.register(Region)
admin.site.register(Comuna)
admin.site.register(Tienda)
#admin.site.register(TipoSucursal)
admin.site.register(Sucursal)
admin.site.register(Producto)
admin.site.register(Lista)
admin.site.register(ListaProducto)
